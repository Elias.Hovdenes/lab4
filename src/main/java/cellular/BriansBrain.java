package cellular;
import datastructure.CellGrid;
import java.util.Random;

import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{


    IGrid currentGeneration;



    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        // TODO Auto-generated method stub
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();
		// TODO
		
		
		

		for (int i=0; i<currentGeneration.numRows(); i++){
            for (int j=0; j<currentGeneration.numColumns(); j++){
                CellState aliveOrDead = getNextCell(i, j);
				nextGeneration.set(i,j, aliveOrDead);
            }
        }

		currentGeneration=nextGeneration;
        
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO Auto-generated method stub
        int numNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState status = getCellState(row, col);
		
		
		if (status==CellState.ALIVE){
            return CellState.DYING;
		}
		
		if (status==CellState.DYING){
            return CellState.DEAD;
        }

        if (status==CellState.DEAD && numNeighbors==2){
            return CellState.ALIVE;
        }else{
            return CellState.DEAD;
        }
    
    }

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return currentGeneration.numRows() ;
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return currentGeneration.numColumns() ;
    }

    
    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }

    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int sum= 0;
		//Sjekker di tre cellene under:
		
		//if(row-1<=0)  //Sjekker om
		
		try{
			if (currentGeneration.get(row+1, col+1) == state){
				sum+=1;
			}
		}catch(Exception OutOfRange){}

		try{
			if (currentGeneration.get(row+1, col) == state){
				sum+=1;
			}
		}catch(Exception OutOfRange){}

		try{
			if (currentGeneration.get(row+1, col-1) == state){
				sum+=1;
			}
		}catch(Exception OutOfRange){}

		//Sjekker di to cellene ved siden av:
		

		try{
			if (currentGeneration.get(row, col+1) == state){
				sum+=1;
			}

		}catch(Exception OutOfRange){}

		try{
			if (currentGeneration.get(row, col-1) == state){
				sum+=1;
			}

		}catch(Exception OutOfRange){}


		//Sjekker di tre cellene over:
		try{
			if (currentGeneration.get(row-1, col) == state){
				sum+=1;
			}

		}catch(Exception OutOfRange){}

		try{
			if (currentGeneration.get(row-1, col+1) == state){
				sum+=1;
			}

		}catch(Exception OutOfRange){}

		try{
			if (currentGeneration.get(row-1, col-1) == state){
				sum+=1;
			}

		}catch(Exception OutOfRange){}
		
	
		
		return sum;

	}
    
}
