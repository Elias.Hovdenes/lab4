package gui;

import cellular.BriansBrain;
import cellular.CellAutomaton;
import cellular.GameOfLife;

public class Main {

	public static void main(String[] args) {
		//CellAutomaton ca = new GameOfLife(1000,1000);
		CellAutomaton ca = new BriansBrain(300, 300);
		CellAutomataGUI.run(ca);
	}

}
