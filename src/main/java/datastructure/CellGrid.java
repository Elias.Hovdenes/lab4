package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] grid;

    
    
    
    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.columns = columns;
        this.rows = rows;

        grid = new CellState[rows][columns];

        for (CellState[] row : grid){
            Arrays.fill(row,initialState);
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        

        if (row>numRows() || row < 0 || column>numColumns() || column < 0){
            throw new IndexOutOfBoundsException("Out of Bounds");
        }

        this.grid[row][column] = element;

    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub


        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid grid_copy = new CellGrid(this.rows,this.columns,CellState.DEAD);
        for (int i=0; i<rows; i++){
            for (int j=0; j<columns; j++){
                grid_copy.set(i,j, get(i,j));
            }
        }

        return grid_copy;
    }
    
}
